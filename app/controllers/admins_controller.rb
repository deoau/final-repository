class AdminsController < ApplicationController
  def index
#   @daily = get_reports(LineItem.where(created_at: Date.today.beginning_of_day..Date.today.end_of_day))
  end
  
  private

  def get_reports(line_item)
  	top_ten = []
  	products = Product.all
  	products.each do |p| 
  		number_of_buyers = 0
  		line_item.each do |i|  
  			if p.id == i.product_id
  				number_of_buyers += 1 
  			end
  		end
  		top_ten.push({:id => p.id, :name => p.name, :rating => number_of_buyers})
  	end
  	return top_ten.sort_by { |r| r[:rating] }.reverse.take(10)
  end
end
