class ReportsController < ApplicationController
    
	def index
	    @daily = get_reports(OrderLine.where(created_at: Date.today.beginning_of_day..Date.today.end_of_day))
	  	@weekly = get_reports(OrderLine.where(created_at: Date.today.beginning_of_week..Date.today.end_of_week))
	  	@monthly = get_reports(OrderLine.where(created_at: Date.today.beginning_of_month..Date.today.end_of_month))
	end

	private

	def get_reports(order_lines)
		top_ten = []
		products = Product.all
		products.each do |p| 
			number_of_buyers = 0
			qeach = 0
			order_lines.each do |o|  
				if p.id == o.product_id
					number_of_buyers += 1
					qeach += o.quantity
				end
			end
			unless number_of_buyers.zero?
				top_ten.push({:id => p.id, :name => p.name, :rating => number_of_buyers, :quantityeach => qeach})
			end
		end
		return top_ten.sort_by { |r| r[:quantityeach] }.reverse.take(10)
	end
	
end
