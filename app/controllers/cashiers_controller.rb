class CashiersController < ApplicationController
    before_action :set_cashier, only: [:update, :edit, :destroy, :show]

  # def my_cashiers
  #   @cashiers = current_admin.cashiers
  # end
  
  def index
    @cashiers = Cashier.all
  end
  
  def new
    @cashier = current_admin.cashiers.new
  end
  
  def create
    @cashier = current_admin.cashiers.new(cashier_params)
    @cashier.status = true
    
    if @cashier.save
      redirect_to cashiers_path, notice: "Cashier successfully added."
    else
      render :new
    end
  end
  
  def destroy
    if @cashier.status == true
      @cashier.status = false
    else
      @cashier.status = true
    end
    
    if @cashier.save
      redirect_to cashiers_path, notice: "Cashier successfully updated."
    end
  end
  
  def edit
  end
  
  def update
    if @cashier.update(cashier_params)
      redirect_to cashiers_path, notice: "Cashier successfully updated."
    else
      render :edit
    end
  end
  
  def show
  end
  
  private
    def cashier_params
      params.require(:cashier).permit!
    end
    
    def set_cashier
      @cashier = Cashier.find_by(id: params[:id])
      redirect_to cashiers_path, notice: "Cashier not found." if @cashier.nil?
    end
end
