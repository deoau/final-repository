class ProductsController < ApplicationController
  before_action :set_product, only: [:update, :edit, :destroy, :show]

  # def my_products
  #   @products = current_admin.products
  # end
  
  def index
    @products = Product.all
  end
  
  def new
    @product = Product.new
  end
  
  def create
    @product = Product.new(product_params)
    @product.status = true
    
    if @product.save
      redirect_to products_path, notice: "Product successfully added."
    else
      render :new
    end
  end
  
  def destroy
    if @product.status == true
      @product.status = false
    else
      @product.status = true
    end
    
    if @product.save
      redirect_to products_path, notice: "Product successfully updated."
    end
  end
  
  def edit
  end
  
  def update
    if @product.update(product_params)
      redirect_to products_path, notice: "Product successfully updated."
    else
      render :edit
    end
  end
  
  def show
  end
  
  private
    def product_params
      params.require(:product).permit!
    end
    
    def set_product
      @product = Product.find_by(id: params[:id])
      redirect_to products_path, notice: "Product not found." if @product.nil?
    end
end
