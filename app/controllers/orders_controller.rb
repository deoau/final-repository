class OrdersController < ApplicationController
    before_action :set_order, only: [:show, :edit, :update, :destroy]
  
  def index
    @orders = Order.all
  end
  
  def my_orderlist
    @orders = []
    @revenue = 0.0
    current_admin.cashiers.each do |cashier|
      cashier.orders.each do |order|
        @orders << order
        @revenue += order.total_price
        # return @orders.sort_by { |r| r[orders.id] }
      end
    end
  end
  
  def order_summary
    @order = current_cashier.orders.new(order_params)
    
    tp = 0
    @order.order_lines.each do |c|
    tp += c.product.price * c.quantity
    end
    
    @order.total_price = tp
  end
  
  def new
    @order = current_cashier.orders.new
  end
  
  def create
    @order = current_cashier.orders.new(order_params)

    tp = 0
    @order.order_lines.each do |c|
    tp += c.product.price * c.quantity
    end
    
    @order.total_price = tp
    
    if @order.save
      redirect_to order_path(@order), notice: "Order successfully created."
    else
      render :new
    end
  end
  
  def destroy
    @order.destroy
    redirect_to orders_path
  end
  
  # def destroy
  #   if @order.status == true
  #     @order.status = false
  #   else
  #     @order.status = true
  #   end
    
  #   if @order.save
  #     redirect_to orders_path, notice: "Order successfully updated."
  #   end
  # end
  
  def edit
  end
  
  def update
    if @order.update(order_params)
      redirect_to order_order_lines_path(@order), notice: "Order paid successfully."
    else
      render :edit
    end
  end
  
  # def update
  #   if @order.update(order_params)
  #     redirect_to orders_path, notice: "Order successfully updated."
  #   else
  #     render :edit
  #   end
  # end
  
  def payment
    @order_lines = @order.order_lines
  end
  
  def show
    @order = Order.find_by(id: params[:id])
    
    @change = @order.cash.to_s.to_d - @order.total_price.to_s.to_d
    
    if !@order.present?
      redirect_to orders_path
    end
  end
  
  private
    def order_params
      params.require(:order).permit!
    end
    
    def set_order
      @order = Order.find_by(id: params[:id])
      redirect_to orders_path, notice: "Order not found." if @order.nil?
    end
end