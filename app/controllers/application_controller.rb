class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  def after_sign_in_path_for(resource)
    if resource.is_a?(Admin)
      authenticated_admin_path
    else
      authenticated_cashier_path
    end
  end
  
  def after_sign_out_path_for(resource_or_scope)
    root_path
  end
  
  def to_s
    return @name
  end
  # helper_method :resource, :resource_name, :devise_mapping
end
