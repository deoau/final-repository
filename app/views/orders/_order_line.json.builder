json.extract! orderline, :product_id, :price, :quantity
json.url orderline_url(orderline, format: :json)
