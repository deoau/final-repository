json.extract! order_line, :product_id, :price, :quantity
json.url order_line_url(order_line, format: :json)
