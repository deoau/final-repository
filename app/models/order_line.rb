class OrderLine < ApplicationRecord
    belongs_to :order, required: false
    belongs_to :product
    
    before_save :compute_price

    private

    def compute_price
    	self.price = self.quantity * self.product.price
    end
end
