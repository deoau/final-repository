class Order < ApplicationRecord
    belongs_to :cashier
    has_many :order_lines#, inverse_of: :product
    accepts_nested_attributes_for :order_lines, 
    reject_if: :all_blank, allow_destroy: true
    
    
    after_create :compute_total_price

    private 

   	def compute_total_price
   		total_price = 0
   		self.order_lines.each do |lines|
   			total_price += lines.price
   		end

   		self.update_attributes(:total_price => total_price)
   	end
end
