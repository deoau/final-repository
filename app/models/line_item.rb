class LineItem < ApplicationRecord
  belongs_to :product
  belongs_to :order

  before_save :compute_total_price,
  			  :descrease_total_stock
  after_save :recompute_orders
  after_destroy :recompute_orders
  before_destroy :increase_total_stock

  validates :quantity, presence: true,
  					   numericality: { greater_than: 0 }

  private

  def compute_total_price
  	self.price = self.product.price * self.quantity
  end

  def descrease_total_stock
  	self.product.update_attributes(stock_items: (self.product.stock_items-self.quantity))
  end

  def increase_total_stock
  	self.product.update_attributes(stock_items: (self.product.stock_items+self.quantity))
  end

  def recompute_orders
  	total = self.order.compute_total
  	item_total = self.order.compute_item_total
  	self.order.update_attributes(total: total, item_total: item_total)
  end
end
