Rails.application.routes.draw do
  get 'orders/:id/payment', to: "orders#payment", as: "order_payment"
  resources :order_lines
  devise_scope :admin do
    authenticated :admin do
      get 'my_orderlist', to: "orders#my_orderlist", as: :my_orderlist
		  resources :products
		  resources :cashiers
		  resources :reports
      root 'admins#index', as: :authenticated_admin
    end
  end
  
  devise_scope :cashier do
    authenticated :cashier do
      resources :orders
      # resources :order_lines
      post 'order_summary', to: "orders#order_summary", as: "order_summary"
      # resources :cashiers
      root 'cashiers#home', as: :authenticated_cashier
    end
  end
  
  devise_for :cashiers
  devise_for :admins
  root 'publics#index'
end