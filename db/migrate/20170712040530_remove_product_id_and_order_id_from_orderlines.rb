class RemoveProductIdAndOrderIdFromOrderlines < ActiveRecord::Migration[5.1]
  def self.up
    remove_column :orderlines, :product_id
    remove_column :orderlines, :order_id
  end
end
