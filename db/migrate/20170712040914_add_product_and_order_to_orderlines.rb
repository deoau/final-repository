class AddProductAndOrderToOrderlines < ActiveRecord::Migration[5.1]
  def change
    add_reference :orderlines, :product, index: true
    add_reference :orderlines, :order, index: true
  end
end
