class RemoveCashiedIdFromOrders < ActiveRecord::Migration[5.1]
  def self.up
    remove_column :orders, :cashier_id
  end
end
