class RenameOrderuscoreLinesToOrderLines < ActiveRecord::Migration[5.1]
  def change
    rename_table :order_lines, :orderlines
  end
end
