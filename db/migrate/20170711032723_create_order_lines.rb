class CreateOrderlines < ActiveRecord::Migration[5.1]
  def change
    create_table :orderlines do |t|
      t.integer :product_id
      t.integer :order_id
      t.integer :quantity
      t.decimal :price

      t.timestamps
    end
  end
end
