class AddCashierToOrders < ActiveRecord::Migration[5.1]
  def change
    add_reference :orders, :cashier, index: true
  end
end
