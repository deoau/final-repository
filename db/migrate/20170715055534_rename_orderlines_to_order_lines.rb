class RenameOrderlinesToOrderLines < ActiveRecord::Migration[5.1]
  def change
    rename_table :orderlines, :order_lines
  end
end
